import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFF367CBA);
const kPrimaryLightColor = Color(0xFFFE0E0E0);
const kNavigationColor = Color(0xFF302E2E);

//Loader Message
class LoaderMessage {
  static String fetchProperties = 'Fetching Dogs List';
  
}
