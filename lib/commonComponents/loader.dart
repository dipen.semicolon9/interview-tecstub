import 'package:flutter/material.dart';
import './constant.dart';

class Loader extends StatelessWidget {
  final bool isLoading;
  final String text;
  Loader({
    this.isLoading = false,
    this.text = '',
  });
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return isLoading
        ? Positioned(
            top: 0,
            left: 0,
            child: Container(
              width: size.width,
              height: size.height,
              padding: EdgeInsets.only(top: 250),
              color: Colors.white.withOpacity(0.8),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 50.0,
                    width: 50.0,
                    child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation(kPrimaryColor),
                        strokeWidth: 3.0),
                  ),
                  SizedBox(height: 20),
                  Text(
                    text,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 25,
                        color: Colors.black),
                  )
                ],
              ),
            ),
          )
        : Positioned(top: 0, left: 0, child: Container());
  }
}
