import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import './providers/dogs.dart';
import './screens/dashboard_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<DogsList>(
      create: (context) => DogsList([]),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Dogs App',
        theme: ThemeData(
          scaffoldBackgroundColor: Colors.white,
        ),
        home: DashboardScreen(),
      ),
    );
  }
}
