import 'package:flutter/material.dart';

class DogModel {
  String id;
  String url;
  int width;
  int height;
  String name;
  String lifeSpan;

  DogModel({
    @required this.id,
    @required this.url,
    @required this.height,
    @required this.width,
    @required this.name,
    @required this.lifeSpan,
  });

  static convertToModel(json) {
    print('ConverToModel:$json');
    var breeds = json['breeds'];
    var dogName = '';
    var doglifeSpan = '';
    if (breeds.length >= 1) {
      dogName = breeds[0]['name'];
      doglifeSpan = breeds[0]['life_span'];
    }
    final obj = DogModel(
      id: json['id'],
      url: json['url'],
      width: json['width'],
      height: json['height'],
      name: dogName,
      lifeSpan: doglifeSpan,
    );

    return obj;
  }
}
