import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

// import '../constant.dart';
import '../models/http_exception.dart';
import '../models/dog.dart';

class DogsList with ChangeNotifier {
  final List<DogModel> dogList;
  DogsList(
    this.dogList,
  ) {
    fetchDogs();
  }

  Future<void> fetchDogs() async {
    final url = 'https://api.thedogapi.com/v1/images/search?limit=50';
    try {
      final response = await http.get(
        url,
      );
      final jsonRespone = json.decode(response.body);
      this.dogList.clear();
      if (jsonRespone.length >= 1) {
        for (var deviceDict in jsonRespone) {
          this.dogList.add(
                DogModel.convertToModel(deviceDict),
              );
        }
      }
      notifyListeners();
    } on TimeoutException catch (_) {
      print('TIme out');
      throw HttpException('Request Time out');
    } catch (error) {
      print(error);
      throw error;
    }
  }

  Future<void> sortList(assending) async {
    if (assending) {
      this.dogList.sort((a, b) {
        return a.lifeSpan
            .toString()
            .toLowerCase()
            .compareTo(b.lifeSpan.toString().toLowerCase());
      });
    } else {
      this.dogList.sort((a, b) {
        return b.lifeSpan
            .toString()
            .toLowerCase()
            .compareTo(a.lifeSpan.toString().toLowerCase());
      });
    }
    notifyListeners();
  }
}
