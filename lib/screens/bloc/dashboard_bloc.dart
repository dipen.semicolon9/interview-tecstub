import 'dart:async';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';

import './dashboard_event.dart';
import '../../providers/dogs.dart';
import '../../models/http_exception.dart';

class DashboardBloc {
  bool _isLoading = false;
  bool _sortByAsc = false;

  final _isLoadingStateController = StreamController<bool>();
  StreamSink<bool> get _inLoading => _isLoadingStateController.sink;
  Stream<bool> get loading => _isLoadingStateController.stream;

  final _sortByAscStateController = StreamController<bool>();
  StreamSink<bool> get _insortByAsc => _sortByAscStateController.sink;
  Stream<bool> get sortByAsc => _sortByAscStateController.stream;

  final _dashboardeventController = StreamController<DashboardEvent>();
  Sink<DashboardEvent> get loadingEventSink => _dashboardeventController.sink;

  DashboardBloc() {
    _dashboardeventController.stream.listen(_mapEventToState);
  }
  void _mapEventToState(DashboardEvent event) {}

  void dispose() {
    _dashboardeventController.close();
    _isLoadingStateController.close();
    _sortByAscStateController.close();
  }

  void _showErrorDialog(BuildContext context, String message) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(
        message,
        textAlign: TextAlign.left,
      ),
      duration: Duration(seconds: 2),
    ));
  }

  void updateSorting() {
    _sortByAsc = !_sortByAsc;
  }

  bool get getsortByAsc {
    return _sortByAsc;
  }

  void fetchDogList(context) async {
    _isLoading = true;
    _inLoading.add(_isLoading);
    try {
      await Provider.of<DogsList>(context, listen: false).fetchDogs();
    } on HttpException catch (error) {
      final errorMessage = error.toString();
      _showErrorDialog(context, errorMessage);
    } catch (error) {
      final errorMessage = error.toString();
      _showErrorDialog(context, errorMessage);
    }
    _isLoading = false;
    _inLoading.add(_isLoading);
  }

  void sortList(context) async {
    _sortByAsc = !_sortByAsc;
    _insortByAsc.add(_sortByAsc);
  }
}
