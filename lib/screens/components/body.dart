import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../providers/dogs.dart';
import '../bloc/dashboard_bloc.dart';

import './dashboard_cell.dart';
import '../../commonComponents/loader.dart';

class Body extends StatelessWidget {
  final bloc = DashboardBloc();
  Future<void> _refreshData(context) async {
    await Provider.of<DogsList>(context, listen: false).fetchDogs();
  }

  Future<void> sortData(context) async {
    bloc.updateSorting();
    await Provider.of<DogsList>(context, listen: false)
        .sortList(bloc.getsortByAsc);
  }

  @override
  Widget build(BuildContext context) {
    bloc.fetchDogList(context);
    return SafeArea(
      child: Consumer<DogsList>(
        builder: (ctx, deviceListManager, _) {
          print('Dashboard : ${deviceListManager.dogList}');
          return RefreshIndicator(
            child: Stack(alignment: Alignment.topCenter, children: <Widget>[
              ListView.builder(
                  padding: EdgeInsets.all(8),
                  itemCount: deviceListManager.dogList.length,
                  itemBuilder: (BuildContext context, int index) {
                    return DashboardCell(
                      dog: deviceListManager.dogList[index],
                      // bloc: DashbaordCellBloc(),
                    );
                  }),
              StreamBuilder(
                  stream: bloc.loading,
                  initialData: false,
                  builder: (BuildContext cntx, AsyncSnapshot<bool> snapshot) {
                    return Loader(
                      isLoading: snapshot.data,
                      text: '',
                    );
                  })
            ]),
            onRefresh: () {
              return _refreshData(context);
            },
          );
        },
      ),
    );
  }
}
