import 'package:flutter/material.dart';
import '../../models/dog.dart';
import 'package:cached_network_image/cached_network_image.dart';

class DashboardCell extends StatelessWidget {
  final DogModel dog;

  const DashboardCell({
    Key key,
    @required this.dog,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            constraints: BoxConstraints(
              minHeight: 100,
            ),
            child: Card(
              color: Color(0xFFF0F0F0),
              child: Stack(
                children: [
                  CachedNetworkImage(
                    imageUrl: dog.url,
                    fit: BoxFit.fill,
                    placeholder: (context, url) => Center(
                      child: SizedBox(
                        width: 30,
                        height: 30,
                        child: CircularProgressIndicator(),
                      ),
                    ),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      padding: EdgeInsets.all(15),
                      width: double.infinity,
                      color: Colors.black.withOpacity(0.5),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            dog.name,
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w600),
                          ),
                          Text(
                            dog.lifeSpan,
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.normal),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
