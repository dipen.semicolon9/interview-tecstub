import 'package:flutter/material.dart';
// import 'package:provider/provider.dart';
// import '../providers/dogs.dart';
import './components/body.dart';

class DashboardScreen extends StatelessWidget {
  final body = Body();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Dashboard'),
        actions: [
          new IconButton(
            icon: new Icon(
              Icons.sort_by_alpha,
            ),
            onPressed: () async {
              body.sortData(context);
            },
          )
        ],
      ),
      body: body,
    );
  }
}
